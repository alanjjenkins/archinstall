#/bin/bash
HOSTNAME="work-vm"

function hostname()
{
    echo "$HOSTNAME" > /etc/hostname
}

function timezone()
{
    ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
}

function enable_locales()
{
    LOCALES=( en_GB en_US )
    for locale in "${LOCALES[@]}"
    do
        sed -i '/^#$locale /s/^#//' /etc/locale.gen
    done

    locale-gen

    # main locale is the first argument to the locales array
    echo "LANG=$LOCALES[0].UTF-8" > /etc/locale.conf
}

function set_keymap()
{
    echo "KEYMAP=uk" > /etc/vconsole.conf
}

function mkinitcpio()
{
    bash mkinitcpio -p linux
}

function install_bootloader()
{
    bootctl install --path=/boot
}

function install_yaourt()
{
    grep 'archlinuxfr' /etc/pacman.conf
    if [ ! "$?" == "0" ]
    then
        (
        cat <<'END'
[archlinuxfr]
Server = http://repo.archlinux.fr/$arch
SigLevel = Never
END
        ) >> /etc/pacman.conf
        pacman -Sy --noconfirm yaourt
    fi
}

function install_packages()
{
    PACKAGES=(
        ansible
        aria2
        atool
        bc
        bower
        brackets
        cmus
        dmenu
        docker
        docker-compose
        elinks
        feh
        freemind
        gimp
        git
        gnome
        google-chrome
        highlight
        htop
        i3-wm
        jre8-openjdk
        lightdm
        lightdm-gtk-greeter
        mediainfo
        mosh
        npm
        openbox
        pavucontrol
        perl-image-exiftool
        ranger
        st-solarized-powerline
        terminator
        the_silver_searcher
        tmux
        ttf-google-fonts-git
        vim
        w3m
        xorg-apps
        xorg-server
        xorg-server-utils
    )

    for PACKAGE in "${PACKAGES[@]}"
    do
        yaourt -Sy --noconfirm "$PACKAGE"
    done
}

hostname
timezone
enable_locales
set_keymap
mkinitcpio
install_bootloader
install_yaourt
install_packages
